# Using certbot to request a letentcrip SSL-Certification

## Requirement
- certbot
- python3-certbot-nginx *for nginx webserver* or
- python3-certbot-apache *for apache webserver*

Run this commande to install it.

    $ apt install certbot python3-certbot-[apache/nginx]

- A no ssl configuration of the webiste should be already created.

## Request SSL for the first time.

Request the certificat with `certbot --[apache/nginx]`.


    # Give your email adress
    # Agree the Term of Service : a
    # refuse to publish your email adress 'N'
    # Select for wed site for ssl certifiacte: '1,2,..' to select specify server or leave blank to select all server


Refresh the webpage on the server and should be rederict to the `https` page.

To see what happen look into the comfig page of each server.

## Renew a certificat

Each certificat have by default `three month`. therefore it have to be renewed each time.
To archive this job we use `certbot renew` command.
Run 

    certbot renew -n -q

to renew the all expire certificat in a non-intiractive mode `-n` and quitely `-q`


## Configure  crontjob to renew certificat

run `crontab -e ` to open the table of cron job of the server. Add  the following 


    0 6 * * * 0 certbot -n -q

to strat the commend each sunday at 6:00 am.

## Help for this Documentation

- [How To Renew Your Let's Encrypt SSL Certificate AUTOMATICALLY (with crontab)](https://www.youtube.com/watch?v=J6LTMsa5bPM&list=PL_vyuxE-AO-Bto5NFfiE7iKuPLMtAh_SV&index=9)
- [Let's Encrypt Explained: Free SSL](https://www.youtube.com/watch?v=jrR_WfgmWEw)
- [How to Install a Free SSL Certificate with Let's Encrypt (on Nginx and Apache websites)](https://www.youtube.com/watch?v=PGDx3xxLGgA&list=PL_vyuxE-AO-Bto5NFfiE7iKuPLMtAh_SV&index=10)
